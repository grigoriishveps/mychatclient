import axios from "axios";

export const SET_LOADING ="SET_LOADING";
export const LOGIN_USER ="LOGIN_USER";
export const SIGN_IN_USER ="SIGN_IN_USER";
export const LOGOUT_USER ="LOGOUT_USER";
export const GET_ACCOUNT_SUCCESS ="GET_ACCOUNT_SUCCESS";

export const getAuthHeader = () => {
    return 'Bearer ' + localStorage.getItem('tc-token');
}

export const setLoading = (value) => {
    return (dispatch, getState) => {
        dispatch({type: SET_LOADING, data: value});
    }
};

export function whoami() {
    return function (dispatch) {
        const token = localStorage.getItem('tc-token');
        if (!token) {
            // dispatch({
            //     type: APP_READY,
            // });
            return;
        }

        dispatch(setLoading(true));
        return axios({
            method: 'get',
            url: 'http://localhost:3000/me',
            headers: {Authorization: getAuthHeader()}
        }).then(response => {
            dispatch(setLoading(false));
            dispatch({
                type: GET_ACCOUNT_SUCCESS,
                data: response.data.user
            });
            // dispatch({
            //     type: APP_READY,
            // });
        }).catch(err => {
            //localStorage.removeItem('bt-token');
            // errorCatcher(err, dispatch, 'Authorization error - please login again');
            // dispatch({
            //     type: APP_READY,
            // });
        });
    }
}
export const login =(login, password) =>{
    return (dispatch)=>{
        axios.post('http://localhost:3000/login', {login, password}).then(response=>{
            const token = response.data.token;
            localStorage.setItem("tc-token", token);
            dispatch({
                type: LOGIN_USER,
                user: response.data.user
            })
        })
    }
}

export const logout = () =>{
    return (dispatch)=>{
        localStorage.removeItem("tc-token");
        dispatch({
            type: LOGOUT_USER,
        })
    }
}

export const signIn =(data) =>{
    return (dispatch)=>{
        axios.post('http://localhost:3000/sign-in', data).then(response=>{
            // const token = response.data.token;
            // localStorage.setItem("tc-token", token);
            // dispatch({
            //     type: SIGN_IN_USER,
            //     user: response.data.user
            // })
        })
    }
}
