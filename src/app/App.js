import React, {Component} from "react";
import MainWrapper from "../components/MainWrapper";
import {BrowserRouter} from "react-router-dom";
import '../styles/style.scss'
import '../../node_modules/bootstrap/dist/css/bootstrap.css'
export default class App extends Component{

    render(){
        return(<BrowserRouter>
            <MainWrapper/>
        </BrowserRouter>)
    }
}
