import appReducer from "../reducers/appReducer";
import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import {whoami} from "../actions/appAction";


const reducers = combineReducers({
    app: appReducer,
})
const store = createStore(reducers,applyMiddleware(thunk));

store.dispatch(whoami());
export default store;
