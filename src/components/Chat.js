import React, { useEffect, useRef, useState } from 'react'
// получаем класс IO
import io from 'socket.io-client'
import { nanoid } from 'nanoid'
import {bindActionCreators} from "redux";
import {login, logout} from "../actions/appAction";
import {connect} from "react-redux";
import moment from "moment";
// наши хуки
// import { useLocalStorage, useBeforeUnload } from 'hooks'

// адрес сервера
// требуется перенаправление запросов - смотрите ниже
const SERVER_URL = 'http://localhost:3000'

// хук принимает название комнаты
const Chat = (props, element = window) => {
    // локальное состояние для пользователей
    const [users, setUsers] = useState([])
    // локальное состояние для сообщений
    const [messages, setMessages] = useState([])
    const [mes, setMes] = useState('')
    // создаем и записываем в локальное хранинище идентификатор пользователя
    // const [username] = useLocalStorage('username')
    const roomId = 1;
    // useRef() используется не только для получения доступа к DOM-элементам,
    // но и для хранения любых мутирующих значений в течение всего жизненного цикла компонента

    const socketRef = useRef(null)
    // useEffect(
    //     () => {
    //         const isSupported =  element.addEventListener;
    //         if (!isSupported) return;
    //         const eventListener = event => (e)=>{
    //             if (e.keyCode === 13) {
    //                 this.props.onSearch(this.state.term);
    //             }
    //         };
    //         // Add event listener
    //         element.addEventListener('eventName', eventListener);
    //         return () => {
    //             element.removeEventListener('eventName', eventListener);
    //         };
    //     },
    //     [] // Re-run if eventName or element changes
    // );


    useEffect(() => {
        // создаем экземпляр сокета, передаем ему адрес сервера
        // и записываем объект с названием комнаты в строку запроса "рукопожатия"
        // socket.handshake.query.roomId
        socketRef.current = io(SERVER_URL, {
            query: { roomId }
        })

        // отправляем событие добавления пользователя,
        // в качестве данных передаем объект с именем и id пользователя
        socketRef.current.emit('user:add', { username:props.user?.name , userId: props.userId })

        // обрабатываем получение списка пользователей
        socketRef.current.on('users', (users) => {
            // обновляем массив пользователей
            setUsers(users)
        })

        // отправляем запрос на получение сообщений
        socketRef.current.emit('message:get')

        // обрабатываем получение сообщений
        socketRef.current.on('messages', (messages) => {
            // определяем, какие сообщения были отправлены данным пользователем,
            // если значение свойства "userId" объекта сообщения совпадает с id пользователя,
            // то добавляем в объект сообщения свойство "currentUser" со значением "true",
            // иначе, просто возвращаем объект сообщения
            // const newMessages = messages.map((msg) =>
            //     msg.userId === userId ? { ...msg, currentUser: true } : msg
            // )
            // обновляем массив сообщений
            // setMessages(newMessages)
            setMessages(messages)
        })


        return () => {socketRef.current.disconnect()}
  //  }, [roomId, userId, username])
    }, [roomId])

    // функция отправки сообщения
    // принимает объект с текстом сообщения и именем отправителя
    const sendMessage = () => {
        // добавляем в объект id пользователя при отправке на сервер
        if(mes =='')
            return;
        console.log({
            userId: props.userId,
            text: mes,
            senderName: props.user?.name
        })
        socketRef.current.emit('message:add', {
            userId: props.userId,
            text: mes,
            senderName: props.user?.name
        })
        setMes('');
    }

    // функция удаления сообщения по id
    const removeMessage = (id) => {
        socketRef.current.emit('message:remove', id)
    }
    const formatTime = (time)=>{
        return moment.utc(time).format("HH:mm")
    }

    // отправляем на сервер событие "user:leave" перед перезагрузкой страницы
    // useBeforeUnload(() => {
    //     socketRef.current.emit('user:leave', userId)
    // })

    const handleKeyPress = (e)=>{
        if (e.code === "Enter") {
            sendMessage();
        }
    }
    // хук возвращает пользователей, сообщения и функции для отправки удаления сообщений
    //return { users, messages, sendMessage, removeMessage }
    return(<div className="card chat-wrapper" >
            <div className="card-header">

            </div>
            <div className="card-body">
                {messages.map((elem)=>{
                        return <div className="d-flex flex-row chat-message-box" key={elem['_id']}>
                            <div className="" >
                                <h6>{elem.senderName}</h6>
                                <p>{elem.text}</p>
                            </div>
                            <div className="ml-auto mr-1 mt-2">
                                <small> {formatTime(elem['createdAt'])}</small>
                            </div>
                        </div>
                    }
                )}
                <div className="input-group chat-send-box">
                    <input className="form-control" value={mes} onKeyPress={handleKeyPress} onChange={(e)=>{setMes(e.target.value)}}/>
                    <div className="input-group-append">
                        <button className="btn btn-primary"  onClick={sendMessage}> Отправить </button>
                    </div>
                </div>
            </div>

    </div>
    )
}

function mapStateToProps(state) {
    return {
        userId: state.app.user?.userId,
        user: state.app.user,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
