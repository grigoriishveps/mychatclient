import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {logout} from "../actions/appAction";
import {connect} from "react-redux";

class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-light bg-white">
                <div className="navbar-left">

                </div>
                <div className="d-flex flex-row justify-content-end navbar-right">
                    <button className="btn btn-danger" onClick={this.props.logout}>Выход</button>
                </div>
            </nav>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        logout
    }, dispatch);
}

export default connect(null, mapDispatchToProps)(Header);
