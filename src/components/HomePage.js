import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {login, logout} from "../actions/appAction";
import {connect} from "react-redux";
import Chat from "./Chat";
import "../styles/style.scss";

class HomePage extends Component {
    render() {
        return (
            <>
                <div className="container">
                    <Chat/>
                </div>
            </>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch);
}


export default connect(null, mapDispatchToProps)(HomePage);


