import React, {Component, useState} from 'react';
import '../styles/login.css';
import {Link} from "react-router-dom";
import  {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {login} from "../actions/appAction";
function Login(props) {

    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    return (
        <div className="login-wrapper">
            <div className="login-box">
                <form>
                    <h5 className="text-head">Вход</h5>
                    <div className="form-group">
                        <label className="login-text" htmlFor="exampleInputEmail1">Логин</label>
                        <input type="text"
                               className="form-control login-input"
                               id="exampleInputEmail1"
                               value={login}
                               onChange={(event)=>setLogin(event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label className="login-text" htmlFor="passwordHelp">Пароль</label>
                        <input type="text"
                               className="form-control login-input"
                               id="passwordHelp"
                               value={password}
                               onChange={(event)=>setPassword(event.target.value)}/>
                    </div>
                </form>
                    <div className="d-flex justify-content-between">
                        <button className='login-button-sign-up' onClick={()=>{props.history.push('/sign-in')}}>Зарегистрироваться</button>
                        <button className='login-button-sign-in' onClick={()=>{props.login(login, password)}}>Войти</button>
                    </div>


            </div>
        </div>
    );
}

// function Login() {
//     return (
//         <div className="login-wrapper">
//             <div className="login-box d-flex flex-column">
//                 <h5>Вход</h5>
//
//                 <div className="d-flex justify-content-between mb-40">
//                     <div className="form-check form-check-inline">
//                         <input className="form-check-input login-checkbox" type="checkbox" id="inlineCheckbox1" value="option1" />
//                         <label className="form-check-label login-label" htmlFor="inlineCheckbox1">Запомнить</label>
//                     </div>
//                     <Link to='' className='login-link'>Забыли пароль?</Link>
//                 </div>
//
//                 {/*<Button onClick={}></Button>*/}
//             </div>
//         </div>
//     );
//
// }


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login
    }, dispatch);
}


export default connect(null, mapDispatchToProps)(Login);
