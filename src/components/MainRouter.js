import React, {Component} from 'react';
import Login from "./Login";
import SignIn from "./SignIn";
import HomePage from "./HomePage";
import {Route, Switch, Redirect} from "react-router-dom";
import {connect} from "react-redux";

const PrivateRoute = ({data, component: Component, ...rest}) => (
    <Route {...rest}

        render={props => {
            if (!data.user) {
                return <Redirect to={{pathname: "/login", state: {from: props.location}}}/>;
            }
            return <Component {...props} key={props.location.pathname} />;
        }}
    />
);
const NotLoggedRoute = ({data, component: Component, ...rest}) => (
    <Route {...rest}
           render={props => {
               if (data.user) {
                   return <Redirect to={{pathname: "/", state: {from: props.location}}}/>;
               }
               return <Component {...props} key={props.location.pathname}/>;
           }}
    />
);

const MainRouter =(props)=>  {
    const {user} = props;
    const data = {
        user: user
    };

    return (
        <Switch>
            {/*<NotLoggedRoute data={data} exact path='/auth-page' component={LoginPage}/>*/}
            <NotLoggedRoute data={data} exact path='/login' component={Login}/>
            <NotLoggedRoute data={data} exact path='/sign-in' component={SignIn}/>
            <PrivateRoute data={data} exact path='/' component={HomePage}/>
            {/*<Route exact path=':id/' component={HomePage}/>*/}
            {/*<Route exact path=':id/mes/:id_mes' component={HomePage}/>*/}
            {/*<Route exact path='/' component={Login}/>*/}
        </Switch>
    );

}
function mapStateToProps(state) {
    return {
        user: state.app.user,
        // appReady: state.app.appReady
    };
}

export default connect(mapStateToProps, null, null, {pure: false})(MainRouter);

