import React, {Component} from 'react';
import Footer from "./Footer";
import Header from "./Header";
import MainRouter from "./MainRouter";

class MainWrapper extends Component {
    render() {
        return (
            <div>
                <Header/>
                <MainRouter/>
                <Footer/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.app.user,
        // appReady: state.app.appReady
    };
}

export default MainWrapper;
