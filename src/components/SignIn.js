import React, {Component, useState} from 'react';
import {bindActionCreators} from "redux";
import {login, signIn} from "../actions/appAction";
import {connect} from "react-redux";

const SignIn = (props)=> {
    const [name, setName] = useState('')
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    const signIn = async()=>{
        props.signIn({name, login, password});
    }

    return (
        <div className="signin-wrapper">
            <div className="login-box signin-box">
                <form>
                    <h5 className="text-head">Регистрация</h5>
                    <div className="form-group">
                        <label className="login-text" htmlFor="exampleInputName">Имя пользователя</label>
                        <input type="text"
                               className="form-control login-input"
                               id="exampleInputName"
                               value={name}
                               onChange={(event)=>setName(event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label className="login-text" htmlFor="exampleInputEmail1">Логин</label>
                        <input type="text"
                               className="form-control login-input"
                               id="exampleInputEmail1"
                               value={login}
                               onChange={(event)=>setLogin(event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label className="login-text" htmlFor="passwordHelp">Пароль</label>
                        <input type="text"
                               className="form-control login-input"
                               id="passwordHelp"
                               value={password}
                               onChange={(event)=>setPassword(event.target.value)}/>
                    </div>
                </form>
                <div className="d-flex justify-content-between mt-4">
                    <button className='login-button-sign-up ' onClick={()=>{props.history.push('/login')}}>Вернуться</button>
                    <button className='login-button-sign-in' onClick={signIn}>Регистрация</button>
                </div>

            </div>
        </div>
    );
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        signIn
    }, dispatch);
}


export default connect(null, mapDispatchToProps)(SignIn);
