import App from "./app/App";
import {Provider} from "react-redux";
import {render} from 'react-dom';
import store from "./app/store";
import React from "react";

render(<Provider store={store}>
    <App/>
</Provider>, document.getElementById("app"))
// render(<App/>, document.getElementById("app"))
