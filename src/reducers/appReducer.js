import {GET_ACCOUNT_SUCCESS, LOGIN_USER, LOGOUT_USER, SET_LOADING} from "../actions/appAction";


const initialState = {
    user:null,
    loading: false
}

export default function (state=initialState, action){
    switch (action.type){
        case SET_LOADING:
            return {
                ...state,
                loading: action.data
            };
        case LOGIN_USER:
            return{
                ...state,
                user: action.user
            }
        case LOGOUT_USER:
            return{
                ...state,
                user: null
            }
        case GET_ACCOUNT_SUCCESS:
            return{
                ...state,
                user: action.data
            }
        default:
            return state;
    }
}
