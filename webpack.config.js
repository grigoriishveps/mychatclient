const webpack = require("webpack");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const ErrorOverlayPlugin = require('error-overlay-webpack-plugin')
const eslintFormatter = require('react-dev-utils/eslintFormatter');
var path = require('path');
const autoprefixer = require('autoprefixer');
module.exports = {
    entry: "./src/index.js", // входная точка - исходный файл
    output:{
        path: path.resolve(__dirname, 'dist'),     // путь к каталогу выходных файлов - папка public
        filename: "bundle.js"       // название создаваемого файла
    },
    mode:"development",
    devServer: {
        //contentBase: "./dist",
        compress: true,
        overlay:true,
        historyApiFallback: true,
        port: 8081,
        hot: true,
        open: true,
        // proxy: {
        //     context: () => true,
        //     target: 'http://localhost:3000',
        // },
    },
    devtool: 'cheap-module-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            title:"test webpack",
            template: "./public/index.html"
        }),
        new CleanWebpackPlugin(),
        new ErrorOverlayPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ],
    module:{
        rules:[   //загрузчик для jsx

            {
                test: /\.(js|jsx)$/, // определяем тип файлов
                exclude: /(node_modules)/,  // исключаем из обработки папку node_modules
                loader: "babel-loader",   // определяем загрузчик
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"]    // используемые плагины
                }
            }
            ,
            {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader','sass-loader']
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"]
            }

        ]
    }
}
